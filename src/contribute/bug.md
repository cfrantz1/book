# How to report bugs

1. First thing to do will be to check our [gitlab issues](https://gitlab.com/veloren/veloren/issues) if this bug is already known.
2. If not [create an issue](https://gitlab.com/veloren/veloren/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) and try to describe the bug as good as possible, include screenshots if needed.
3. Upload the log file ``voxygen.log`` for additional information which can help us identify the issue 
which is located where you started the game or in ``%appdata%/veloren/``.
(**Note:** make sure no sensitive information is included in the log files)
4. Submit the issue

Incase you do not want to create an gitlab account you can join our [discord](https://discord.gg/BvQuGze)
and report the bug there.
